#ifndef _JSON_QUERY_H
#define _JSON_QUERY_H

#include <string>
#include <utility>
#include <map>
#include <list>
#include <nlohmann/json.hpp>

using json = nlohmann::json;
namespace emumba
{
    namespace training
    {
        class json_query
        {
        public:
            json make_personal_json_object(const std::string &name) const;                               // make json object to store in personal map
            json make_account_json_object(const std::string &account_id) const;                          // make json object to store in account map
            void write_to_output_file(json json_object) const;                                           // write result to output file
            std::string float_to_string(float val, int per);                                             // convert float to string of specific percision for comparison
            bool store_personal_data(const std::string &name);                                           // just for unit tests to access private members
            bool store_account_data(const std::string &name);                                            // // just for unit tests to access private members
            std::pair<std::string /* key */, std::string /* value */> validate_input(std::string query); // to validate query
            bool initialize_data_base(const bool &use_ordered_map);                                                                 // read and store data from json file to database
            bool find_in_database(const std::string &key, const std::string &value, const bool &use_ordered_map);                                   // self explanatory
            void poll_queries();                                                                         // continious function for polling

            void set_name(const std::string &name); //  setters of personal data
            void set_age(const unsigned short &age);
            void set_city(const std::string &city);
            void set_cordinates(const float &latitude, const float &longitude);

            std::string get_name(const std::string &name) const; //  getters of personal data
            unsigned short get_age(const std::string &name) const;
            std::string get_city(const std::string &name) const;
            std::pair<float, float> get_cordinates(const std::string &name) const;

            void set_bank(const std::string &bank); //  setters of account data
            void set_account_id(const std::string &account_id);
            void set_balance(const float &balance);
            void set_currency(const std::string &currency);

            std::string get_bank(const std::string &account_id) const; //  getters of account data
            std::string get_account_id(const std::string &account_id) const;
            float get_balance(const std::string &account_id) const;
            std::string get_currency(const std::string &account_id) const;

        private:
            struct personal_data_struct
            {
                std::string name;
                unsigned short age;
                std::string city;
                std::pair<float, float> cordinates;
            } personal_data;
            std::list<personal_data_struct> personal_info_list;

            struct account_data_struct
            {
                std::string bank;
                std::string account_id;
                float balance;
                std::string currency;
                json balance_j;
            } account_data;
            std::list<account_data_struct> account_info_list;

            std::map<std::string /*person's name*/, personal_data_struct *> personal_info_map;
            std::unordered_map<std::string /*person's name*/, personal_data_struct *> personal_info_unordered_map;
            std::map<std::string /*account id*/, account_data_struct *> accounts_info_map;
            std::unordered_map<std::string /*account id*/, account_data_struct *> accounts_info_unordered_map;
        };
    }
}

#endif