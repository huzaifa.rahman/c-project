# C++ Project

This repo contains the C++ project of training session.

### How to configure, build and run

**Configure** :
- Clone this repo using ``git clone --recurse-submodules https://gitlab.com/huzaifa.rahman/c-project.git`` and go to the newly created local dir with ``cd``.
- Run ``mkdir build`` to create a build dir for configuration and build files. 
- Navigate to ``<project_root_dir>/build``.
- Run ``cmake ..`` to congigure in ``Release`` or ``cmake -DDEBUG=ON ..`` to congigure in ``Debug`` mode.

**Build** : 
- In ``<project_root_dir>/build`` run ``make -j`` build the executable or run ``cmake --build .``.

**Execute / Run** : 
- In ``<project_root_dir>/build`` run ``./query_app`` or ``./query_app_debug`` to execute in release or debug executable respectively.
- To run unit tests, run ``./json_test`` in ``<project_root_dir>/build``.
- For benchmarking, run ``./bench_marking`` in ``<project_root_dir>/build``.

