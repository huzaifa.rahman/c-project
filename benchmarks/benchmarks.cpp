#include <benchmark/benchmark.h>
#include "json_query.h"

using namespace emumba::training;

// testing data reading into ordered map
static void BM_Data_initialization_ordered_map(benchmark::State &state)
{
    json_query json_database;
    for (auto _ : state)
        json_database.initialize_data_base(true);
}
BENCHMARK(BM_Data_initialization_ordered_map); // using ordered map

// testing data reading into unordered map
static void BM_Data_initialization_unordered_map(benchmark::State &state)
{
    json_query json_database;
    for (auto _ : state)
        json_database.initialize_data_base(false);
}
BENCHMARK(BM_Data_initialization_unordered_map); // using unordered map

// testing data reading from ordered map
static void BM_Data_reading_ordered_map(benchmark::State &state)
{
    json_query json_database;
    json_database.initialize_data_base(true);
    for (auto _ : state)
        json_database.find_in_database("age", "30", true);
}
BENCHMARK(BM_Data_reading_ordered_map); // using ordered map

// testing data reading from unordered map
static void BM_Data_reading_unordered_map(benchmark::State &state)
{
    json_query json_database;
    json_database.initialize_data_base(false);
    for (auto _ : state)
        json_database.find_in_database("age", "30", false);
}
BENCHMARK(BM_Data_reading_unordered_map); // using unordered map

// command to compare results
// ../3rd_party/benchmark/tools/compare.py filters ./bench_marking BM_Data_initialization_ordered_map BM_Data_initialization_unordered_map
// (new - old) / |old|

static void BM_MakingOfPersonalJsonObject(benchmark::State &state)
{
    json_query json_database;
    json_database.initialize_data_base(true);
    for (auto _ : state)
        json_database.make_personal_json_object("FirstName1 LastName1");
}
BENCHMARK(BM_MakingOfPersonalJsonObject);

static void BM_MakingOfAccountJsonObject(benchmark::State &state)
{
    json_query json_database;
    json_database.initialize_data_base(true);
    for (auto _ : state)
        json_database.make_account_json_object("XYZ-423467");
}
BENCHMARK(BM_MakingOfAccountJsonObject);

static void BM_WritingToOutputFile(benchmark::State &state)
{
    json_query json_database;
    json_database.initialize_data_base(true);
    json json_obj = json_database.make_personal_json_object("FirstName1 LastName1");
    for (auto _ : state)
        json_database.write_to_output_file(json_obj);
}
BENCHMARK(BM_WritingToOutputFile);

static void BM_QueryValidation(benchmark::State &state)
{
    json_query json_database;
    for (auto _ : state)
        json_database.validate_input("\"age\" : \"30\"");
}
BENCHMARK(BM_QueryValidation);
