#include "gtest/gtest.h"
#include "json_query.h"

using namespace std;
using namespace emumba::training;

class JsonTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
        json_database.initialize_data_base(true);
    }

    json_query json_database;
};

TEST_F(JsonTest, DataSearchTest)
{
    EXPECT_TRUE(json_database.find_in_database("age", "30", true)) << "age not found in database";
    EXPECT_FALSE(json_database.find_in_database("age", "40", true)) << "age found in database";
}

TEST_F(JsonTest, InputFileTest)
{
    EXPECT_TRUE(json_database.initialize_data_base(true)) << "input file does not exist";
}

TEST_F(JsonTest, StoringPersonalDataTest)
{
    json_database.set_name("ali");
    json_database.set_city("CityA");
    json_database.set_age(30);
    json_database.set_cordinates(30.10, 25.5);
    EXPECT_TRUE(json_database.store_personal_data("ali")) << "date is not stored";
    EXPECT_EQ(json_database.get_name("ali"), "ali");
    EXPECT_EQ(json_database.get_city("ali"), "CityA");
    EXPECT_EQ(json_database.get_age("ali"), 30);
    EXPECT_FLOAT_EQ(json_database.get_cordinates("ali").first, 30.10);
    EXPECT_FLOAT_EQ(json_database.get_cordinates("ali").second, 25.5);
}

TEST_F(JsonTest, StoringAccountDataTest)
{
    json_database.set_bank("XYZ");
    json_database.set_account_id("XYZ-1234");
    json_database.set_currency("PKR");
    json_database.set_balance(101000.10);
    EXPECT_TRUE(json_database.store_account_data("XYZ-1234")) << "account date is not stored";
    EXPECT_EQ(json_database.get_bank("XYZ-1234"), "XYZ");
    EXPECT_EQ(json_database.get_account_id("XYZ-1234"), "XYZ-1234");
    EXPECT_EQ(json_database.get_currency("XYZ-1234"), "PKR");
    EXPECT_FLOAT_EQ(json_database.get_balance("XYZ-1234"), 101000.10);
}

int main()
{
    testing::InitGoogleTest();
    return RUN_ALL_TESTS();
}
