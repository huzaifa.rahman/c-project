# globbing to collect files
file(GLOB SRCS "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp")

add_library(
    json_query
    ${SRCS}
)

target_include_directories(json_query PUBLIC "${PROJECT_SOURCE_DIR}/include" "${PROJECT_SOURCE_DIR}/third_party/json/include")

target_link_libraries(${PROJECT_NAME} PUBLIC nlohmann_json)