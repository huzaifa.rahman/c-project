#include "json_query.h"
#include "nlohmann/json.hpp"
#include <fstream>
#include <unistd.h>
#include <string>
#include <vector>
#include <cmath>

using namespace emumba::training;
using namespace std;
using json = nlohmann::json;

void json_query::set_name(const string &name)
{
    personal_data.name = name;
}
void json_query::set_age(const unsigned short &age)
{
    personal_data.age = age;
}
void json_query::set_city(const string &city)
{
    personal_data.city = city;
}
void json_query::set_cordinates(const float &latitude, const float &longitude)
{
    personal_data.cordinates.first = latitude;
    personal_data.cordinates.second = longitude;
}

string json_query::get_name(const string &name) const
{
    string ret = "";
    auto search = personal_info_map.find(name);
    if (search != personal_info_map.end())
    {
        if (search->second->name.length())
        {
            ret = search->second->name;
        }
    }
    return ret;
}
unsigned short json_query::get_age(const string &name) const
{
    unsigned short ret = 0;
    auto search = personal_info_map.find(name);
    if (search != personal_info_map.end())
    {
        if (search->second->age)
        {
            ret = search->second->age;
        }
    }
    return ret;
}
string json_query::get_city(const string &name) const
{
    string ret = "";
    auto search = personal_info_map.find(name);
    if (search != personal_info_map.end())
    {
        if (search->second->city.length())
        {
            ret = search->second->city;
        }
    }
    return ret;
}
pair<float, float> json_query::get_cordinates(const string &name) const
{
    pair<float, float> ret = {0.0, 0.0};
    auto search = personal_info_map.find(name);
    if (search != personal_info_map.end())
    {
        if (search->second->cordinates.first && search->second->cordinates.second)
        {
            ret = search->second->cordinates;
        }
    }
    return ret;
}

void json_query::set_bank(const string &bank)
{
    account_data.bank = bank;
}
void json_query::set_account_id(const string &account_id)
{
    account_data.account_id = account_id;
}
void json_query::set_balance(const float &balance)
{
    account_data.balance = balance;
}
void json_query::set_currency(const string &currency)
{
    account_data.currency = currency;
}

string json_query::get_bank(const string &account_id) const
{
    string ret = "";
    auto search = accounts_info_map.find(account_id);
    if (search != accounts_info_map.end())
    {
        if (search->second->bank.length())
        {
            ret = search->second->bank;
        }
    }
    return ret;
}
string json_query::get_account_id(const string &account_id) const
{
    string ret = "";
    auto search = accounts_info_map.find(account_id);
    if (search != accounts_info_map.end())
    {
        if (search->second->account_id.length())
        {
            ret = search->second->account_id;
        }
    }
    return ret;
}
float json_query::get_balance(const string &account_id) const
{
    float ret = 0;
    auto search = accounts_info_map.find(account_id);
    if (search != accounts_info_map.end())
    {
        if (search->second->balance)
        {
            ret = search->second->balance;
        }
    }
    return ret;
}
string json_query::get_currency(const string &account_id) const
{
    string ret = "";
    auto search = accounts_info_map.find(account_id);
    if (search != accounts_info_map.end())
    {
        if (search->second->currency.length())
        {
            ret = search->second->currency;
        }
    }
    return ret;
}

bool json_query::store_personal_data(const string &name)
{
    personal_info_list.push_back(personal_data_struct(personal_data));
    return personal_info_map.insert({name, &personal_data}).second;
}

bool json_query::store_account_data(const string &account_id)
{
    account_info_list.push_back(account_data_struct(account_data));
    return accounts_info_map.insert({account_id, &account_data}).second;
}

bool json_query::initialize_data_base(const bool &use_ordered_map)
{
    bool return_status = false;
    // read a JSON file
    ifstream personal_data_file("personal_data.json");
    if (personal_data_file.good())
    {
        json j;

        personal_data_file >> j;
        if (j.is_object())
        {
            if (j["Account Holders Info"].is_array())
            {
                j = j["Account Holders Info"];
                if (!j.empty())
                {
                    for (auto i = j.begin(); i != j.end(); ++i)
                    {
                        if (i.value().is_object() && !i.value().empty())
                        {
                            if (i.value()["name"].is_string())
                            {
                                set_name(i.value()["name"]);
                            }
                            if (i.value()["age"].is_number_integer())
                            {
                                set_age(i.value()["age"]);
                            }
                            if (i.value()["city"].is_string())
                            {
                                set_city(i.value()["city"]);
                            }
                            if (i.value()["coordinates"].is_object() && !i.value()["coordinates"].empty())
                            {
                                if (i.value()["coordinates"]["lat"].is_number_float() && i.value()["coordinates"]["long"].is_number_float())
                                {
                                    set_cordinates(i.value()["coordinates"]["lat"], i.value()["coordinates"]["long"]);
                                }
                            }

                            personal_info_list.push_back(personal_data_struct(personal_data));
                            if (use_ordered_map)
                            {
                                personal_info_map.insert({personal_data.name, &personal_info_list.back()});
                            }
                            else
                            {
                                personal_info_unordered_map.insert({personal_data.name, &personal_info_list.back()});
                            }

                            if (i.value()["accounts"].is_array() && !i.value()["accounts"].empty())
                            {
                                for (auto k = i.value()["accounts"].begin(); k != i.value()["accounts"].end(); ++k)
                                {
                                    if (k.value().is_object() && !k.value().empty())
                                    {
                                        if (k.value()["bank"].is_string())
                                        {
                                            set_bank(k.value()["bank"]);
                                        }
                                        if (k.value()["account id"].is_string())
                                        {
                                            set_account_id(k.value()["account id"]);
                                        }
                                        if (k.value()["balance"].is_number_float())
                                        {
                                            set_balance(k.value()["balance"]);
                                        }
                                        if (k.value()["currency"].is_string())
                                        {
                                            set_currency(k.value()["currency"]);
                                        }

                                        account_info_list.push_back(account_data_struct(account_data));
                                        if (use_ordered_map)
                                        {
                                            accounts_info_map.insert({account_data.account_id, &account_info_list.back()});
                                        }
                                        else
                                        {
                                            accounts_info_unordered_map.insert({account_data.account_id, &account_info_list.back()});
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return_status = true;
                }
            }
        }
    }
    personal_data_file.close();
    return return_status;
}

json json_query::make_personal_json_object(const string &name) const
{
    return json::object(
        {{"name", get_name(name)},
         {"age", get_age(name)},
         {"city", get_city(name)},
         {"coordinates", {{"lat", get_cordinates(name).first}, {"long", get_cordinates(name).second}}}});
}

json json_query::make_account_json_object(const string &account_id) const
{
    return json::object(
        {{"bank", get_bank(account_id)},
         {"account id", get_account_id(account_id)},
         {"balance", get_balance(account_id)},
         {"currency", get_currency(account_id)}});
}

void json_query::write_to_output_file(json json_object) const
{
    ofstream output_file("query_result.json");
    output_file << setw(4) << json_object << endl;
    output_file.close();
}

pair<string, string> json_query::validate_input(string query)
{
    string query_key, query_value;

    if (count(query.begin(), query.end(), '"') != 4 || count(query.begin(), query.end(), ':') != 1)
    {
        return pair<string, string>{"", ""};
    }

    query_key = query.substr(0, query.find(":"));
    if (count(query_key.begin(), query_key.end(), '"') != 2)
    {
        return pair<string, string>{"", ""};
    }
    query_key = query_key.substr(query_key.find("\"") + 1, query_key.length());
    query_key = query_key.substr(0, query_key.find("\""));

    query_value = query.substr(query.find(":") + 1, query.length());
    query_value = query_value.substr(query_value.find("\"") + 1, query_value.length());
    query_value = query_value.substr(0, query_value.find("\""));

    return pair<string, string>{query_key, query_value};
}

string json_query::float_to_string(float val, int per)
{
    float perc = powf(10.0, per);
    val = val * perc;
    if (val < 0)
        val = ceil(val - 0.5);
    val = floor(val + 0.5);
    string float_str(to_string(val));
    float_str.insert(float_str.find(".") - per, ".");
    return float_str;
}

bool json_query::find_in_database(const string &key, const string &value, const bool &use_ordered_map)
{
    bool return_val = false;
    vector<string> keys_vector = {"name", "age", "city", "lat", "long", "bank", "account id", "currency", "balance", "clear"};
    if (find(keys_vector.begin(), keys_vector.end(), key) != keys_vector.end())
    {
        if (!key.compare("name"))
        {
            if (use_ordered_map && personal_info_map.find(value) != personal_info_map.end())
            {
                write_to_output_file(make_personal_json_object(value));
                return_val = true;
            }
            else if (!use_ordered_map && personal_info_unordered_map.find(value) != personal_info_unordered_map.end())
            {
                write_to_output_file(make_personal_json_object(value));
                return_val = true;
            }
        }
        else if (!key.compare("age"))
        {
            json array = json::array();
            if (use_ordered_map)
            {
                for (auto it = personal_info_map.begin(); it != personal_info_map.end(); ++it)
                {
                    if (it->second->age == static_cast<unsigned short>(strtoul(value.c_str(), NULL, 0)))
                    {
                        array.push_back(make_personal_json_object(it->second->name));
                        return_val = true;
                    }
                }
            }
            else
            {
                for (auto it = personal_info_unordered_map.begin(); it != personal_info_unordered_map.end(); ++it)
                {
                    if (it->second->age == static_cast<unsigned short>(strtoul(value.c_str(), NULL, 0)))
                    {
                        array.push_back(make_personal_json_object(it->second->name));
                        return_val = true;
                    }
                }
            }
            if (!array.empty())
            {
                write_to_output_file(array);
            }
        }
        else if (!key.compare("city"))
        {
            json array = json::array();
            if (use_ordered_map)
            {
                for (auto it = personal_info_map.begin(); it != personal_info_map.end(); ++it)
                {
                    if (!it->second->city.compare(value))
                    {
                        array.push_back(make_personal_json_object(it->second->name));
                        return_val = true;
                    }
                }
            }
            else
            {
                for (auto it = personal_info_unordered_map.begin(); it != personal_info_unordered_map.end(); ++it)
                {
                    if (!it->second->city.compare(value))
                    {
                        array.push_back(make_personal_json_object(it->second->name));
                        return_val = true;
                    }
                }
            }
            if (!array.empty())
            {
                write_to_output_file(array);
            }
        }
        else if (!key.compare("lat"))
        {
            json array = json::array();
            if (use_ordered_map)
            {
                for (auto it = personal_info_map.begin(); it != personal_info_map.end(); ++it)
                {
                    string database_lat = float_to_string(it->second->cordinates.first, value.length() - value.find(".") - 1);
                    if (!database_lat.substr(0, value.length()).compare(value))
                    {
                        array.push_back(make_personal_json_object(it->second->name));
                        return_val = true;
                    }
                }
            }
            else
            {
                for (auto it = personal_info_unordered_map.begin(); it != personal_info_unordered_map.end(); ++it)
                {
                    string database_lat = float_to_string(it->second->cordinates.first, value.length() - value.find(".") - 1);
                    if (!database_lat.substr(0, value.length()).compare(value))
                    {
                        array.push_back(make_personal_json_object(it->second->name));
                        return_val = true;
                    }
                }
            }
            if (!array.empty())
            {
                write_to_output_file(array);
            }
        }
        else if (!key.compare("long"))
        {
            json array = json::array();
            if (use_ordered_map)
            {
                for (auto it = personal_info_map.begin(); it != personal_info_map.end(); ++it)
                {
                    string database_long = float_to_string(it->second->cordinates.second, value.length() - value.find(".") - 1);
                    string trimmed_string = database_long.substr(0, value.length());
                    if (!trimmed_string.compare(value))
                    {
                        array.push_back(make_personal_json_object(it->second->name));
                        return_val = true;
                    }
                }
            }
            else
            {
                for (auto it = personal_info_unordered_map.begin(); it != personal_info_unordered_map.end(); ++it)
                {
                    string database_long = float_to_string(it->second->cordinates.second, value.length() - value.find(".") - 1);
                    string trimmed_string = database_long.substr(0, value.length());
                    if (!trimmed_string.compare(value))
                    {
                        array.push_back(make_personal_json_object(it->second->name));
                        return_val = true;
                    }
                }
            }
            if (!array.empty())
            {
                write_to_output_file(array);
            }
        }
        else if (!key.compare("account id"))
        {
            if (use_ordered_map && accounts_info_map.find(value) != accounts_info_map.end())
            {
                write_to_output_file(make_account_json_object(value));
                return_val = true;
            }
            else if (!use_ordered_map && accounts_info_unordered_map.find(value) != accounts_info_unordered_map.end())
            {
                write_to_output_file(make_account_json_object(value));
                return_val = true;
            }
        }
        else if (!key.compare("bank"))
        {
            json array = json::array();
            if (use_ordered_map)
            {
                for (auto it = accounts_info_map.begin(); it != accounts_info_map.end(); ++it)
                {
                    if (!it->second->bank.compare(value))
                    {
                        array.push_back(make_account_json_object(it->second->account_id));
                        return_val = true;
                    }
                }
            }
            else
            {
                for (auto it = accounts_info_unordered_map.begin(); it != accounts_info_unordered_map.end(); ++it)
                {
                    if (!it->second->bank.compare(value))
                    {
                        array.push_back(make_account_json_object(it->second->account_id));
                        return_val = true;
                    }
                }
            }
            if (!array.empty())
            {
                write_to_output_file(array);
            }
        }
        else if (!key.compare("currency"))
        {
            json array = json::array();
            if (use_ordered_map)
            {
                for (auto it = accounts_info_map.begin(); it != accounts_info_map.end(); ++it)
                {
                    if (!it->second->currency.compare(value))
                    {
                        array.push_back(make_account_json_object(it->second->account_id));
                        return_val = true;
                    }
                }
            }
            else
            {
                for (auto it = accounts_info_unordered_map.begin(); it != accounts_info_unordered_map.end(); ++it)
                {
                    if (!it->second->currency.compare(value))
                    {
                        array.push_back(make_account_json_object(it->second->account_id));
                        return_val = true;
                    }
                }
            }
            if (!array.empty())
            {
                write_to_output_file(array);
            }
        }
        else if (!key.compare("balance"))
        {
            json array = json::array();
            if (use_ordered_map)
            {
                for (auto it = accounts_info_map.begin(); it != accounts_info_map.end(); ++it)
                {
                    string database_balance = float_to_string(it->second->balance, value.length() - value.find(".") - 1);
                    string trimmed_string = database_balance.substr(0, value.length());
                    if (!trimmed_string.compare(value))
                    {
                        array.push_back(make_account_json_object(it->second->account_id));
                        return_val = true;
                    }
                }
            }
            else
            {
                for (auto it = accounts_info_unordered_map.begin(); it != accounts_info_unordered_map.end(); ++it)
                {
                    string database_balance = float_to_string(it->second->balance, value.length() - value.find(".") - 1);
                    string trimmed_string = database_balance.substr(0, value.length());
                    if (!trimmed_string.compare(value))
                    {
                        array.push_back(make_account_json_object(it->second->account_id));
                        return_val = true;
                    }
                }
            }
            if (!array.empty())
            {
                write_to_output_file(array);
            }
        }
        else if (!key.compare("clear"))
        {
            if (!value.compare("file"))
            {
                write_to_output_file(json::object());
                return_val = true;
            }
        }
    }
    return return_val;
}

// TODO comment on code
void json_query::poll_queries()
{
    ifstream query_input_file;
    string last_query_key, last_query_value, query;

    while (true)
    {
        query_input_file.open("query.txt");
        if (query_input_file.is_open())
        {
            getline(query_input_file, query);
            pair<string, string> query_pair(validate_input(query));
            if (query_pair.first.length() == 0 || query_pair.second.length() == 0 ||
                (last_query_key == query_pair.first && last_query_value == query_pair.second))
            {
                sleep(2);
                query_input_file.close();
                continue;
            }
            last_query_key = query_pair.first;
            last_query_value = query_pair.second;

            find_in_database(query_pair.first, query_pair.second, true);
        }
        sleep(2);
        query_input_file.close();
    }
}