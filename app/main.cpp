#include "json_query.h"

using namespace emumba::training;

int main()
{
    int return_val = 1;                       // default value
    json_query json_database;                 // create an instance to store and poll data
    if (json_database.initialize_data_base(true)) // if initialization succeeds
    {
        json_database.poll_queries(); // this is a perpetual function
    }
    return return_val;
}